﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace layouts.Views
{
    public partial class FlexLayoutStackDemoPage : ContentPage
    {
        public FlexLayoutStackDemoPage()
        {
            InitializeComponent();

            int count = 1;

            myButton.Clicked += (sender, e) =>
            {
                var label = new Label();
                label.Text = "" + count;
                count++;
                label.Margin = new Thickness(10);
                flexLayout.Children.Add(label);

            };
        }
    }
}
