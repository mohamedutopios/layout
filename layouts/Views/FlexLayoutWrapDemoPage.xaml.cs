﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace layouts.Views
{
    public partial class FlexLayoutWrapDemoPage : ContentPage
    {
        public FlexLayoutWrapDemoPage()
        {
            InitializeComponent();

            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            /*
            var random = new Random();

            for (int i = 0; i < 98; i++)
            {
                var box = new BoxView();


                int r = random.Next(256);
                int g = random.Next(256);
                int b = random.Next(256);

                int size = random.Next(60, 100);

                box.BackgroundColor = Color.FromRgb(r, g, b);
                box.WidthRequest = size;
                box.HeightRequest = size;
                box.Margin = new Thickness(5);
                myFlexLayout.Children.Add(box);
            }
            */

        }
    }
}
